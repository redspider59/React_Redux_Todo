import React,{Component} from 'react';
import './App.css';
import Posts from './components/posts'
import PostForm from './components/postForm';
import {Provider} from 'react-redux';

import store from './store';

class App extends Component {
  render(){
    return (
      //Provider wraps the elements on the return 
      //it takes the store which holds the state
      <Provider store={store}>
          <div className="App">
            <PostForm/>
            <br/>
            <Posts/>
          </div>
      </Provider>
      
    );

  }
    
  
}

export default App;
