This is some other information regarding the codes

For the basics of Redux and middleware boilerplate setup please refer to the following link of my previous repository:
https://github.com/aarck/React-Redux/tree/master/src

In this Basic project I set up the redux Boilerplate for a Todo_app using  **https://jsonplaceholder.typicode.com/posts** API.

Above API is a fake API which can be used to render data on our projects without working on real APIs.

I have used a practice of declaring the PropTypes which is considered good to track the props of any component.

Seperated **ACTIONS** , **REDUCERS** and  **COMPONENTS**  is better to work on a good boilerplate.

Used fetch command to call the api . In other cases Axios can also be used

