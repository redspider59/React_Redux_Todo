import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
//call action fetchpost
import { fetchPosts} from '../actions/postActions';

export class Posts extends Component {
    componentWillMount(){
        this.props.fetchPosts();
        //seting up the props
    }

    componentWillReceiveProps(nextProps ){
        //when we recieve a property from state this function will run
        if(nextProps.newPost){
            this.props.posts.unshift(nextProps.newPost);
        }
    }
    
    render() {
        const postItems =this.props.posts.map(post =>(
            <div key ={post.id}>
                <h3>{post.title}</h3>
                <h4>{post.body}</h4>
            </div>
        ))
        
        return (
            <div>
                <h1>Posts</h1>
                {postItems}
            </div>
        )
    }
}
Posts.propTypes = {
    fetchPosts:PropTypes.func.isRequired,
    posts: PropTypes.array.isRequired,
    newPost:PropTypes.object
}

const mapStateToProps= state =>({
    posts:state.posts.items,
    newPost:state.posts.item
});

export default connect(mapStateToProps,{fetchPosts})(Posts);
