//IN ROOT REDUCERS WE BASICALLY HAVE TO COMBINE ALL THE REDUCERS

import {combineReducers} from 'redux';
import postReducer from './postReducer';

export default combineReducers({
    posts:postReducer
})