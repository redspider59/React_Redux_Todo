//REDUCERS TAKE CARE OF THE ACTIONS AND DEAL WITH THE STATES INSIDE THE STORE
import { FETCH_POSTS,NEW_POST } from "../actions/types";
import { bindActionCreators } from "redux";

const initialState ={
    items: [],
    item:{}
}
//folowing is defining our reducer
export default function(state= initialState,action){
    switch(action.type){
        case FETCH_POSTS:
            return{
                ...state,
            items:action.payload

            };
        case NEW_POST:
            return{
                ...state,
                item:action.payload
            };
            
        default:
            return state;
    }

}
